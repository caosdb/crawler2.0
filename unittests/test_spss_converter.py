# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2024 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Testing converter for SPSS files."""

import datetime
import importlib
from pathlib import Path

import numpy as np
import pytest

from caoscrawler.converters import ConverterValidationError, SPSSConverter
from caoscrawler.structure_elements import (BooleanElement, DictElement,
                                            Directory, File, FloatElement,
                                            IntegerElement, ListElement,
                                            TextElement)

UNITTESTDIR = Path(__file__).parent


@pytest.fixture
def converter_registry():
    converter_registry: dict[str, dict[str, str]] = {
        "Directory": {
            "converter": "DirectoryConverter",
            "package": "caoscrawler.converters"},
    }

    for key, value in converter_registry.items():
        module = importlib.import_module(value["package"])
        value["class"] = getattr(module, value["converter"])
    return converter_registry


def test_spss_converter(converter_registry):
    converter = SPSSConverter({
        "match": ("sample.sav")
    },
        "ThisConverterNameIsIrrelevant", converter_registry
    )

    spss_dir = UNITTESTDIR / "test_tables" / "spss"
    for sav_file, length, thistype in [
            (File("sample.sav", spss_dir / "sample.sav"), 5, str),
            (File("sample.sav", spss_dir / "sample_large.sav"), 485, int),
    ]:
        m = converter.match(sav_file)
        assert m is not None
        assert len(m) == 0

        children = converter.create_children(None, sav_file)
        assert len(children) == length

        for ii, child in enumerate(children):
            assert child.__class__ == DictElement
            assert child.name == str(ii)
            my_dict = child.value
            assert isinstance(my_dict["mychar"], str)
            assert isinstance(my_dict["mydate"], datetime.date) or np.isnan(my_dict["mydate"])
            assert isinstance(my_dict["dtime"], datetime.datetime) or np.isnan(my_dict["dtime"])
            assert isinstance(my_dict["mytime"], datetime.time) or np.isnan(my_dict["mytime"])
            assert isinstance(my_dict["mylabl"], thistype), f"{type(my_dict['mylabl'])}"
            assert isinstance(my_dict["myord"], thistype), f"{type(my_dict['myord'])}"
