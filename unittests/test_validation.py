#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2024 Alexander Schlemmer <a.schlemmer@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

"""
test validation
"""
from os.path import join
from pathlib import Path

import jsonschema
import linkahead as db
import pytest
from caoscrawler.validator import (convert_record,
                                   load_json_schema_from_datamodel_yaml,
                                   validate)
from jsonschema import ValidationError

UNITTESTDIR = Path(__file__).parent


def test_create_json_schema():
    json = load_json_schema_from_datamodel_yaml(join(UNITTESTDIR, "datamodels", "datamodel.yaml"))
    r = db.Record()
    r.add_parent(name="Dataset")
    r.add_property(name="keywords", value="jakdlfjakdf")
    r.add_property(name="dateModified", value="2024-11-16")

    pobj = convert_record(r)
    # print(yaml.dump(pobj))
    # print(yaml.dump(json[0]))
    assert "Dataset" in json
    jsonschema.validate(pobj, json["Dataset"])

    # Failing test:
    r = db.Record()
    r.add_parent(name="Dataset")
    r.add_property(name="keywordss", value="jakdlfjakdf")
    r.add_property(name="dateModified", value="2024-11-16")

    pobj = convert_record(r)

    with pytest.raises(ValidationError, match=".*'keywords' is a required property.*"):
        jsonschema.validate(pobj, json["Dataset"])


def test_validation():
    """
    Test for the main validation API function `validate`
    """
    json = load_json_schema_from_datamodel_yaml(
        join(UNITTESTDIR, "datamodels", "datamodel.yaml"))
    r1 = db.Record()
    r1.add_parent(name="Dataset")
    r1.add_property(name="keywords", value="jakdlfjakdf")
    r1.add_property(name="dateModified", value="2024-11-16")

    r2 = db.Record()
    r2.add_parent(name="Dataset")
    r2.add_property(name="keywordss", value="jakdlfjakdf")
    r2.add_property(name="dateModified", value="2024-11-16")

    valres = validate([r1, r2], json)
    assert valres[0][0] is True
    assert valres[0][1] is None
    assert not valres[1][0]
    assert isinstance(valres[1][1], ValidationError)
    assert valres[1][1].message == "'keywords' is a required property"
