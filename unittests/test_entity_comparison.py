#!/bin/python
# Tests for entity comparison
# A. Schlemmer, 06/2021

import linkahead as db
import pytest
from pytest import raises

from caoscrawler.crawl import check_identical


def test_compare_entities():
    record1 = db.Record()
    record2 = db.Record()

    assert check_identical(record1, record2)

    record1.add_property(name="type", value="int")
    assert not check_identical(record1, record2)
    assert not check_identical(record2, record1)

    record2.add_property(name="type", value="int")
    assert check_identical(record1, record2)
    record2.get_property("type").value = "int2"
    assert not check_identical(record1, record2)
    record2.get_property("type").value = 4
    assert not check_identical(record1, record2)

    record2.get_property("type").value = "int"
    assert check_identical(record1, record2)
    record2.add_parent(db.RecordType(name="Parent"))
    assert not check_identical(record1, record2)
    record1.add_parent(db.RecordType(name="Parent"))

    # This is confusing, but needed:
    record1.add_property(name="field_with_type", value=42, datatype=db.INTEGER)
    record2.add_property(name="field_with_type", value=42)
    # not identical, because record1 sets the datatype
    assert not check_identical(record1, record2)
    # identical, because record2 sets the datatype
    assert check_identical(record2, record1)
    record2.get_property("field_with_type").datatype = db.INTEGER
    assert check_identical(record1, record2)
    assert check_identical(record2, record1)

    record2.get_property("field_with_type").datatype = db.DOUBLE
    assert not check_identical(record1, record2)
    assert not check_identical(record2, record1)

    # TODO: report this as a hacky workaround (for setting datatype from double to integer):
    record2.get_property("field_with_type").datatype = db.TEXT
    record2.get_property("field_with_type").datatype = db.INTEGER
    assert check_identical(record1, record2)
    assert check_identical(record2, record1)

    record1 = db.File()
    record2 = db.File()

    vals = (("bla bla", "bla bla bla"),
            (1, 2))

    for attribute, values in zip(("description", "name", "path", "id"),
                                 (vals[0], vals[0], vals[0], vals[1])):
        setattr(record1, attribute, values[0])
        assert not check_identical(record1, record2)
        assert not check_identical(record2, record1)
        setattr(record2, attribute, values[1])
        assert not check_identical(record1, record2)
        assert not check_identical(record2, record1)

        setattr(record2, attribute, values[0])
        assert check_identical(record1, record2)
        assert check_identical(record2, record1)

    # currently "file" is not checked by compare_entities

    vals = (("abcd", "bcde"),
            (1, 2))
    # This is confusing, but needed:
    for attribute, values in zip(("_checksum", "_size"),
                                 (vals[0], vals[1])):
        setattr(record1, attribute, values[0])
        # not identical, because record1 sets the datatype
        assert not check_identical(record1, record2)
        # identical, because record2 sets the datatype
        assert check_identical(record2, record1)

        setattr(record2, attribute, values[1])
        assert not check_identical(record1, record2)
        assert not check_identical(record2, record1)

        setattr(record2, attribute, values[0])
        assert check_identical(record1, record2)
        assert check_identical(record2, record1)
