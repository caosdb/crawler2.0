#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2022 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2022 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
from tempfile import NamedTemporaryFile
from unittest.mock import patch

import pytest
import yaml

import caoscrawler
from caoscrawler.scanner import load_definition


def _temp_file_load(txt: str):
    """
    Create a temporary file with txt and load the crawler
    definition using load_definition from Crawler.
    """
    definition = None
    with NamedTemporaryFile(delete=False) as f:
        f.write(txt.encode())
        f.flush()
        definition = load_definition(f.name)
    return definition


def test_warning_if_no_version_specified():
    """Warn if no version is specified in the cfood."""

    # metadata section exists but doesn't specify a version
    definition_text = """
---
metadata:
  name: Something
  description: A cfood that does something
---
SimulationData:
  type: Directory
  match: SimulationData
    """
    with pytest.warns(UserWarning) as uw:
        _temp_file_load(definition_text)

    found = False
    for w in uw:
        if ("No crawler version specified in cfood definition" in w.message.args[0] and
                "Specifying a version is highly recommended" in w.message.args[0]):
            found = True
    assert found

    # metadata section is missing alltogether
    definition_text = """
SimulationData:
  type: Directory
  match: SimulationData
    """

    with pytest.warns(UserWarning) as uw:
        _temp_file_load(definition_text)

    found = False
    for w in uw:
        if ("No crawler version specified in cfood definition" in w.message.args[0] and
                "Specifying a version is highly recommended" in w.message.args[0]):
            found = True
    assert found


@patch("caoscrawler.version.get_caoscrawler_version")
def test_warning_if_version_too_old(get_version):
    """Warn if the cfood was written for an older crawler version."""

    definition_text = """
---
metadata:
  name: Something
  description: A cfood that does something
  crawler-version: 0.2.0
---
SimulationData:
  type: Directory
  match: SimulationData
    """

    get_version.side_effect = lambda: "0.3.0"
    with pytest.warns(UserWarning) as uw:
        _temp_file_load(definition_text)

    found = False
    for w in uw:
        if ("cfood was written for a previous crawler version" in w.message.args[0] and
                "version specified in cfood: 0.2.0" in w.message.args[0] and
                "version installed on your system: 0.3.0" in w.message.args[0]):
            found = True
    assert found

    # higher major
    get_version.side_effect = lambda: "1.1.0"
    with pytest.warns(UserWarning) as uw:
        _temp_file_load(definition_text)

    found = False
    for w in uw:
        if ("cfood was written for a previous crawler version" in w.message.args[0] and
                "version specified in cfood: 0.2.0" in w.message.args[0] and
                "version installed on your system: 1.1.0" in w.message.args[0]):
            found = True
    assert found


@patch("caoscrawler.version.get_caoscrawler_version")
def test_error_if_version_too_new(get_version):
    """Raise error if the cfood requires a newer crawler version."""

    # minor too old
    get_version.side_effect = lambda: "0.1.5"
    definition_text = """
---
metadata:
  name: Something
  description: A cfood that does something
  crawler-version: 0.2.1
---
SimulationData:
  type: Directory
  match: SimulationData
    """
    with pytest.raises(caoscrawler.CfoodRequiredVersionError) as cre:
        _temp_file_load(definition_text)

    assert "cfood definition requires a newer version" in str(cre.value)
    assert "version specified in cfood: 0.2.1" in str(cre.value)
    assert "version installed on your system: 0.1.5" in str(cre.value)

    # major too old
    definition_text = """
---
metadata:
  name: Something
  description: A cfood that does something
  crawler-version: 1.0.1
---
SimulationData:
  type: Directory
  match: SimulationData
    """
    with pytest.raises(caoscrawler.CfoodRequiredVersionError) as cre:
        _temp_file_load(definition_text)

    assert "cfood definition requires a newer version" in str(cre.value)
    assert "version specified in cfood: 1.0.1" in str(cre.value)
    assert "version installed on your system: 0.1.5" in str(cre.value)

    # patch to old
    get_version.side_effect = lambda: "1.0.0"

    with pytest.raises(caoscrawler.CfoodRequiredVersionError) as cre:
        _temp_file_load(definition_text)

    assert "cfood definition requires a newer version" in str(cre.value)
    assert "version specified in cfood: 1.0.1" in str(cre.value)
    assert "version installed on your system: 1.0.0" in str(cre.value)


@patch("caoscrawler.version.get_caoscrawler_version")
def test_matching_version(get_version):
    """Test that there is no warning or error in case the version matches."""

    definition_text = """
---
metadata:
  name: Something
  description: A cfood that does something
  crawler-version: 0.2.1
---
SimulationData:
  type: Directory
  match: SimulationData
    """
    get_version.side_effect = lambda: "0.2.1"
    assert _temp_file_load(definition_text)

    # The version is also considered a match if the patch version of the
    # installed crawler is newer than the one specified in the cfood metadata
    get_version.side_effect = lambda: "0.2.7"
    assert _temp_file_load(definition_text)
