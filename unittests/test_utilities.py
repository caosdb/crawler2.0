#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2023 Alexander Schlemmer <alexander.schlemmer@ds.mpg.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

import pytest
from os.path import sep
from caoscrawler.crawl import split_restricted_path
from caoscrawler.utils import MissingImport, get_shared_resource_link


def test_split_restricted_path():
    assert split_restricted_path("") == []
    assert split_restricted_path(f"{sep}") == []
    assert split_restricted_path(f"test{sep}") == ["test"]
    assert split_restricted_path(f"{sep}test{sep}") == ["test"]
    assert split_restricted_path(f"test{sep}bla") == ["test", "bla"]
    assert split_restricted_path(f"{sep}test{sep}bla") == ["test", "bla"]
    assert split_restricted_path(f"{sep}test1{sep}test2{sep}bla") == ["test1", "test2", "bla"]
    assert split_restricted_path(f"{sep}test{sep}{sep}bla") == ["test", "bla"]
    assert split_restricted_path(f"{sep}{sep}test{sep}bla") == ["test", "bla"]
    assert split_restricted_path(
        f"{sep}{sep}{sep}test{sep}{sep}bla{sep}{sep}{sep}{sep}") == ["test", "bla"]


def test_dummy_class():
    Missing = MissingImport(name="Not Important", hint="Do the thing instead.")
    with pytest.raises(RuntimeError) as err_info_1:
        print(Missing.__name__)
    with pytest.raises(RuntimeError) as err_info_2:
        Missing()
    with pytest.raises(RuntimeError) as err_info_3:
        print(Missing.foo)

    for err_info in (err_info_1, err_info_2, err_info_3):
        msg = str(err_info.value)
        assert "(Not Important)" in msg
        assert msg.endswith("Do the thing instead.")

    MissingErr = MissingImport(name="Not Important", hint="Do the thing instead.",
                               err=ImportError("Old error"))
    with pytest.raises(RuntimeError) as err_info_1:
        print(MissingErr.__name__)
    with pytest.raises(RuntimeError) as err_info_2:
        MissingErr()
    with pytest.raises(RuntimeError) as err_info_3:
        print(MissingErr.foo)

    for err_info in (err_info_1, err_info_2, err_info_3):
        msg = str(err_info.value)
        assert "(Not Important)" in msg
        orig_msg = str(err_info.value.__cause__)
        assert orig_msg == "Old error"


def test_shared_resource_link():

    assert get_shared_resource_link(
        "https://example.com/", "file.txt") == "https://example.com/Shared/file.txt"
    assert get_shared_resource_link(
        "https://example.com", "file.txt") == "https://example.com/Shared/file.txt"
    assert get_shared_resource_link(
        "https://example.com", "path/to/file.txt") == "https://example.com/Shared/path/to/file.txt"
    assert get_shared_resource_link(
        "https://example.com/context-root", "path/to/file.txt") == "https://example.com/context-root/Shared/path/to/file.txt"
    assert get_shared_resource_link(
        "https://example.com/context-root/", "path/to/file.txt") == "https://example.com/context-root/Shared/path/to/file.txt"
