#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""
test identifiable_adapters module
"""

from datetime import datetime
from pathlib import Path
from unittest.mock import MagicMock, Mock, patch

import linkahead as db
import pytest

from caoscrawler.exceptions import InvalidIdentifiableYAML
from caoscrawler.identifiable import Identifiable
from caoscrawler.identifiable_adapters import (CaosDBIdentifiableAdapter,
                                               IdentifiableAdapter,
                                               convert_value)
from caoscrawler.sync_graph import SyncNode

UNITTESTDIR = Path(__file__).parent


def mock_retrieve_RecordType(id, name):
    return {
        "Person": db.RecordType(name="Person"),
        "Keyword": db.RecordType(name="Keyword"),
        "Project": db.RecordType(name="Project"),
        "A": db.RecordType(name="A"),
        "Experiment": db.RecordType(name="Experiment"),
        "Lab": db.RecordType(name="Lab"),
        "Analysis": db.RecordType(name="Analysis"),
        "MetaAnalysis": db.RecordType(name="MetaAnalysis").add_parent("Analysis"),
        "Measurement": db.RecordType(name="Measurement").add_parent("Experiment")
    }[name]


def test_create_query_for_identifiable():
    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(record_type="Person", properties={"first_name": "A", "last_name": "B"}))
    assert query.lower() == "find record 'person' with 'first_name'='a' and 'last_name'='b' "

    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(name="A", record_type="B", properties={
            "c": "c",
            "d": 5,
            "e": 5.5,
            "f": datetime(2020, 10, 10),
            "g": True,
            "h": db.Record(id=1111),
            "i": db.File(id=1112),
            "j": [2222, db.Record(id=3333)]}))
    assert (query == "FIND RECORD 'B' WITH name='A' AND 'c'='c' AND 'd'='5' AND 'e'='5.5'"
            " AND 'f'='2020-10-10T00:00:00' AND 'g'='TRUE' AND 'h'='1111' AND 'i'='1112' AND "
            "'j'='2222' AND 'j'='3333' ")

    # The name can be the only identifiable
    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(name="TestRecord", record_type="TestType"))
    assert query.lower() == "find record 'testtype' with name='testrecord'"

    # With referencing entity (backref)
    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(record_type="Person", backrefs=[14433], properties={'last_name': "B"}))
    assert query.lower() == ("find record 'person' which is referenced by 14433 and with "
                             "'last_name'='b' ")

    # With two referencing entities (backref)
    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(record_type="Person", backrefs=[14433, 333], properties={'last_name': "B"}))
    assert query.lower() == ("find record 'person' which is referenced by 14433 and which is "
                             "referenced by 333 and with 'last_name'='b' ")

    # With single quote in string
    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(record_type="Person", backrefs=[], properties={'last_name': "B'Or"}))
    assert query == ("FIND RECORD 'Person' WITH 'last_name'='B\\'Or' ")

    # With only backref
    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(backrefs=[160], properties={}))
    assert query == ("FIND RECORD  WHICH IS REFERENCED BY 160")

    # With only backref and name
    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(backrefs=[160], name='lo', properties={}))
    assert query == ("FIND RECORD  WHICH IS REFERENCED BY 160 AND WITH name='lo'")

    query = IdentifiableAdapter.create_query_for_identifiable(
        Identifiable(record_type="record type", name="it's weird"))
    assert query == ("FIND RECORD 'record type' WITH name='it\\'s weird'")


@patch("caoscrawler.identifiable_adapters._retrieve_RecordType",
       new=Mock(side_effect=mock_retrieve_RecordType))
def test_load_from_yaml_file():
    ident = CaosDBIdentifiableAdapter()
    ident.load_from_yaml_definition(
        UNITTESTDIR / "test_directories" / "single_file_test_data" / "identifiables.yml"
    )

    person_i = ident.get_registered_identifiable(
        db.Record().add_parent("Person"))
    assert person_i is not None
    assert person_i.get_property("full_name") is not None

    keyword_i = ident.get_registered_identifiable(
        db.Record().add_parent("Keyword"))
    assert keyword_i is not None
    assert keyword_i.get_property("name") is not None

    project_i = ident.get_registered_identifiable(
        db.Record().add_parent("Project"))
    assert project_i is not None
    assert project_i.get_property("project_id") is not None
    assert project_i.get_property("title") is not None


def test_invalid_yaml():
    ident = CaosDBIdentifiableAdapter()
    invalid_dir = UNITTESTDIR / "test_data" / "invalid_identifiable"
    with pytest.raises(InvalidIdentifiableYAML) as exc:
        ident.load_from_yaml_definition(invalid_dir / "identifiable_content_no_list.yaml")
    assert str(exc.value) == "Identifiable contents must be lists, but this was not: Experiment"

    with pytest.raises(InvalidIdentifiableYAML) as exc:
        ident.load_from_yaml_definition(invalid_dir / "identifiable_referenced_no_list.yaml")
    assert str(exc.value) == "'is_referenced_by' must be a list.  Found in: Event"

    with pytest.raises(InvalidIdentifiableYAML) as exc:
        ident.load_from_yaml_definition(invalid_dir / "identifiable_no_str_or_dict.yaml")
    assert str(exc.value) == ("Identifiable properties must be str or dict, but this one was not:\n"
                              "    Experiment/23")


def test_non_default_name():
    ident = CaosDBIdentifiableAdapter()
    identifiable = ident.get_identifiable(SyncNode(db.Record(name="don't touch it")
                                                   .add_parent("Person")
                                                   .add_property(name="last_name", value='Tom'),
                                                   db.RecordType()
                                                   .add_parent(name="Person")
                                                   .add_property(name="last_name")), [])
    assert identifiable.name is None


def test_wildcard_ref():
    ident = CaosDBIdentifiableAdapter()
    rec = (db.Record(name="don't touch it").add_parent("Person")
           .add_property(name="last_name", value='Tom'))
    dummy = SyncNode(db.Record(), None)
    dummy.id = 1
    identifiable = ident.get_identifiable(SyncNode(rec, db.RecordType()
                                                   .add_parent(name="Person")
                                                   .add_property(name="is_referenced_by",
                                                                 value=["*"])),
                                          [dummy]
                                          )
    assert identifiable.backrefs[0] == 1


def test_convert_value():
    # test that string representation of objects stay unchanged. No stripping or so.
    class A():
        def __repr__(self):
            return " a "

    assert convert_value(A()) == " a "


@patch("caoscrawler.identifiable_adapters._retrieve_RecordType",
       new=Mock(side_effect=mock_retrieve_RecordType))
def test_get_identifiable():
    ident = CaosDBIdentifiableAdapter()
    ident.load_from_yaml_definition(UNITTESTDIR / "example_identifiables.yml")
    rec = (db.Record(id=5)
           .add_parent(name="Experiment", id=3)
           .add_property(name="date", value="2022-02-01")
           .add_property(name="result", value="FAIL"))
    se = SyncNode(rec,
                  ident.get_registered_identifiable(rec))
    id_r0 = ident.get_identifiable(se, [])
    assert rec.parents[0].name == id_r0.record_type
    assert rec.get_property("date").value == id_r0.properties["date"]
    assert len(rec.parents) == 1
    assert len(rec.properties) == 2
    assert len(id_r0.properties) == 1

    ident = CaosDBIdentifiableAdapter()
    ident_a = db.RecordType(name="A").add_parent("A").add_property("name").add_property("a")
    ident.register_identifiable("A", ident_a)
    rec = (db.Record(id=5)
           .add_parent(name="A", id=3)
           .add_property(name="a", value="2022-02-01")
           .add_property(name="result", value="FAIL"))
    se = SyncNode(rec, ident.get_registered_identifiable(rec))
    for el in [
        db.Record()
        .add_parent(name="A", id=3)
        .add_property(name="a", value="2022-02-01")
        .add_property(name="result", value="FAIL"),
        db.Record(name='a')
        .add_parent(name="A", id=3)
        .add_property(name="a", value="2022-02-01")
        .add_property(name="result", value="FAIL"),
    ]:
        se.update(SyncNode(el))

    id_r0 = ident.get_identifiable(se, [])
    assert "A" == id_r0.record_type
    assert "2022-02-01" == id_r0.properties["a"]
    assert 'a' == id_r0.name
    assert len(id_r0.properties) == 1

    rec = (db.Record(name='a')
           .add_parent(name="A")
           .add_property(name="a", value="2")
           )
    se = SyncNode(rec, ident.get_registered_identifiable(rec))
    se.update(SyncNode(
        db.Record(name='a')
        .add_parent(name="A")
        .add_property(name="a", value="3")
    ))

    with pytest.raises(RuntimeError):
        id_r0 = ident.get_identifiable(se, [])


@pytest.mark.xfail
def test_retrieve_identified_record_for_identifiable():
    # TODO modify this such that it becomes a test that acutally tests (sufficiently) the
    # retrieve_identified_record_for_identifiable function
    idr_r0_test = ident.retrieve_identified_record_for_identifiable(id_r0)
    idr_r0 = ident.retrieve_identified_record_for_record(r_cur)
    assert idr_r0 == idr_r0_test

    # take the first measurement in the list of records:
    for r in ident.get_records():
        if r.parents[0].name == "Measurement":
            r_cur = r
            break

    id_r1 = ident.get_identifiable(r_cur, [])
    assert r_cur.parents[0].name == id_r1.record_type
    assert r_cur.get_property(
        "identifier").value == id_r1.properties["identifier"]
    assert r_cur.get_property("date").value == id_r1.properties["date"]
    assert r_cur.get_property(
        "project").value == id_r1.properties["project"]
    assert len(r_cur.parents) == 1
    assert len(r_cur.properties) == 4
    assert len(id_r1.properties) == 3

    idr_r1_test = ident.retrieve_identified_record_for_identifiable(id_r1)
    idr_r1 = ident.retrieve_identified_record_for_record(r_cur)
    assert idr_r1 == idr_r1_test
    assert idr_r1 != idr_r0
    assert idr_r1_test != idr_r0_test

    assert len(idr_r1.properties) == 4
    assert r_cur.get_property(
        "responsible").value == idr_r1.get_property("responsible").value
    assert r_cur.description == idr_r1.description


@patch("caoscrawler.identifiable_adapters.get_children_of_rt",
       new=Mock(side_effect=lambda x: [x]))
def test_referencing_entity_has_appropriate_type():
    dummy = db.Record().add_parent("A")
    registered_identifiable = db.RecordType()
    rft = IdentifiableAdapter.referencing_entity_has_appropriate_type
    assert not rft([], registered_identifiable)
    assert not rft(dummy.parents, registered_identifiable)
    registered_identifiable.add_property("is_referenced_by", "B")
    assert not rft(dummy.parents, registered_identifiable)
    registered_identifiable.properties[0].value = ["B", "A"]
    assert rft(dummy.parents, registered_identifiable)
    registered_identifiable.properties[0].value = ["B", "*"]
    assert rft(dummy.parents, registered_identifiable)


@patch("caoscrawler.identifiable_adapters._retrieve_RecordType",
       new=Mock(side_effect=mock_retrieve_RecordType))
def test_get_registered_identifiable():
    # Test the case that the record has a parent for which an identifiable is registered
    ident = CaosDBIdentifiableAdapter()
    ident.load_from_yaml_definition(UNITTESTDIR / "example_identifiables.yml")
    rec = db.Record().add_parent(name="Experiment")
    registered = ident.get_registered_identifiable(rec)
    assert registered is not None
    assert registered.parents[0].name == "Experiment"

    # Test the same but with an additional parent
    rec = db.Record().add_parent(name="Lab").add_parent(name="Experiment")
    registered = ident.get_registered_identifiable(rec)
    assert registered is not None
    assert registered.parents[0].name == "Experiment"

    # Test the same but with an additional parent that also has a registered identifiable
    rec = db.Record().add_parent(name="Analysis").add_parent(name="Experiment")
    with pytest.raises(RuntimeError):
        registered = ident.get_registered_identifiable(rec)

    # Test the same but with an additional parent that has a parent with a registered identifiable
    rec = db.Record().add_parent(name="MetaAnalysis").add_parent(name="Experiment")
    with pytest.raises(RuntimeError):
        registered = ident.get_registered_identifiable(rec)

    # Test the case that the record has a parent for which no identifiable is registered
    # and there is a registered identifiable for a grand parent
    ident = CaosDBIdentifiableAdapter()
    ident.load_from_yaml_definition(UNITTESTDIR / "example_identifiables.yml")
    rec = db.Record().add_parent(name="Measurement")
    registered = ident.get_registered_identifiable(rec)
    assert registered is not None
    assert registered.parents[0].name == "Experiment"
