#!/usr/bin/env python3

# This file is a part of the LinkAhead project.
#
# Copyright (C) 2024 IndiScale GmbH <www.indiscale.com>
# Copyright (C) 2024 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""Test if the scripts work as expected.
"""

from subprocess import run

SCRIPTS = [
    "linkahead-crawler",
    "caosdb-crawler",
    "spss_to_datamodel",
    "csv_to_datamodel",
]


def test_script_loading():
    """Run the scripts with "-h"."""
    for script in SCRIPTS:
        run([script, "-h"], check=True)
