#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2024 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2024 Alexander Schlemmer <a.schlemmer@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

"""
test the zip-file converter
"""
import importlib
import os
from pathlib import Path

import pytest
import yaml
from caoscrawler.converters import DirectoryConverter, ZipFileConverter
from caoscrawler.stores import GeneralStore
from caoscrawler.structure_elements import Directory, File

UNITTESTDIR = Path(__file__).parent


@pytest.fixture
def converter_registry():
    converter_registry: dict[str, dict[str, str]] = {
        "ZipFile": {
            "converter": "ZipFileConverter",
            "package": "caoscrawler.converters"},
    }

    for key, value in converter_registry.items():
        module = importlib.import_module(value["package"])
        value["class"] = getattr(module, value["converter"])
    return converter_registry


@pytest.mark.xfail(
    reason="The example files for PASTA have not yet been updated in:"
    "https://github.com/TheELNConsortium/TheELNFileFormat/tree/master/examples/PASTA"
    "However, there was the announcement that these files are going to follow the"
    "flattened structure soon: https://github.com/TheELNConsortium/TheELNFileFormat/issues/98"
)
def test_zipfile_converter(converter_registry):
    zipfile = File("PASTA.eln", os.path.join(UNITTESTDIR, "eln_files", "PASTA.eln"))
    zip_conv = ZipFileConverter(yaml.safe_load("""
type: ZipFile
match: .*$
"""), "TestZipFileConverter", converter_registry)

    match = zip_conv.match(zipfile)
    assert match is not None

    children = zip_conv.create_children(GeneralStore(), zipfile)
    assert len(children) == 1
    assert children[0].name == "PASTA"

    dir_conv = DirectoryConverter(yaml.safe_load("""
type: Directory
match: ^PASTA$
"""), "TestDirectory", converter_registry)
    match = dir_conv.match(children[0])
    assert match is not None
    children = dir_conv.create_children(GeneralStore(), children[0])
    assert len(children) == 5
    print(children)
    for i in range(2):
        assert isinstance(children[i], Directory)
    for i in range(2, 5):
        assert isinstance(children[i], File)


def test_zipfile_minimal(converter_registry):
    zipfile = File("empty.zip", os.path.join(UNITTESTDIR, "zip_minimal", "empty.zip"))
    zip_conv = ZipFileConverter(yaml.safe_load("""
type: ZipFile
match: .*$
"""), "TestZipFileConverter", converter_registry)

    match = zip_conv.match(zipfile)
    assert match is not None

    children = zip_conv.create_children(GeneralStore(), zipfile)
    assert len(children) == 2

    file_obj = None
    dir_obj = None
    for ch in children:
        if isinstance(ch, File):
            file_obj = ch
        elif isinstance(ch, Directory):
            dir_obj = ch
        else:
            assert False
    assert file_obj is not None and dir_obj is not None
    assert file_obj.name == "empty.txt"

    dir_conv = DirectoryConverter(yaml.safe_load("""
type: Directory
match: ^folder$
"""), "TestDirectory", converter_registry)
    match = dir_conv.match(dir_obj)
    assert match is not None
    children = dir_conv.create_children(GeneralStore(), dir_obj)
    assert len(children) == 3
    for i in range(3):
        assert isinstance(children[i], File)
