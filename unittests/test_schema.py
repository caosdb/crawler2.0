#!/bin/python
# Tests for schema validation
# A. Schlemmer, 06/2021

from os.path import dirname, join

import linkahead as db
import pytest
from importlib_resources import files
from jsonschema.exceptions import ValidationError
from pytest import raises

from caoscrawler import Crawler
from caoscrawler.scanner import load_definition


def rfp(*pathcomponents):
    """
    Return full path.
    Shorthand convenience function.
    """
    return join(dirname(__file__), *pathcomponents)


def test_schema_validation():
    load_definition(rfp("scifolder_cfood.yml"))
    load_definition(rfp("scifolder_extended.yml"))
    load_definition(rfp("record_from_dict_cfood.yml"))

    with raises(ValidationError, match=".*enum.*"):
        load_definition(rfp("broken_cfoods", "broken1.yml"))

    with raises(ValidationError, match=".*required.*"):
        load_definition(rfp("broken_cfoods", "broken_record_from_dict.yml"))

    with raises(ValidationError, match=".*required.*"):
        load_definition(rfp("broken_cfoods", "broken_record_from_dict_2.yml"))
