---
responsible: AuthorE
description: >-
  Code for fitting the predictive model to the
  training data and for predicting the average
  annual temperature for all measurement stations
  for the years 2010 to 2019
sources:
- ../../../ExperimentalData/2020_climate-model-predict/1980-01-01/temperatures-*.csv
- ../../../ExperimentalData/2020_climate-model-predict/1990-01-01/temperatures-*.csv
- ../../../ExperimentalData/2020_climate-model-predict/2000-01-01/temperatures-*.csv
results:
- file: params.json
  description: Model parameters for the best fit to the training set
- file: predictions-201*.csv
  description: Annual temperature predictions with geographical locations
scripts:
- file: model.py
  description: python module with the model equations
- file: fit_parameters.py
  description: Fit model parameters to training data using a basinhopping optimizer
- file: predict.py
  description: Use optimized parameters to simulate average temperatures from 2010 to 2019
...
