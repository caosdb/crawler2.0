---
responsible: AuthorD
description: Average temperatures of the years 2000-2009 as obtained from wheatherdata.example
results:
- file: temperatures-200*.csv
  description: single year averages of all measurement stations with geographic locations
...
