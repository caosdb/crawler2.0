---
responsible: AuthorD
description: Average temperatures of the years 2010-2019 as obtained from wheatherdata.example
results:
- file: temperatures-201*.csv
  description: single year averages of all measurement stations with geographic locations
...
