---
responsible: AuthorD
description: Average temperatures of the years 1980-1989 as obtained from wheatherdata.example
results:
- file: temperatures-198*.csv
  description: single year averages of all measurement stations with geographic locations
...
