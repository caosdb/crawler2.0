#!/bin/python
# Tests for:
# https://gitlab.com/caosdb/caosdb-crawler/-/issues/9
# A. Schlemmer, 06/2021
from pathlib import Path

import pytest
from utils import dircheckstr

# The main function that is affected by this issue:
from caoscrawler.converters import handle_value
from caoscrawler.crawl import Crawler
from caoscrawler.debug_tree import DebugTree
from caoscrawler.scanner import scan_directory
# We need the store for the above function
from caoscrawler.stores import GeneralStore

UNITTESTDIR = Path(__file__).parent


def test_handle_value():
    # Note that we will need this store only, if we also want to test variables substitution:
    store = GeneralStore()

    # This one should work:
    assert handle_value("bla", store) == ("bla", None, "single")

    # These failed:
    assert handle_value(4, store) == (4, None, "single")
    assert handle_value(4.2, store) == (4.2, None, "single")
    assert handle_value(True, store) == (True, None, "single")

    # List test:
    assert handle_value([4, 3, 2], store) == ([4, 3, 2], None, "single")


def test_record_structure_generation():
    dbt = DebugTree()
    scan_directory(UNITTESTDIR / "test_directories" / "examples_article",
                   UNITTESTDIR / "cfoods_scalar.yml",
                   debug_tree=dbt)
    subd = dbt.debug_tree[dircheckstr(
        UNITTESTDIR / "test_directories" / "examples_article", "DataAnalysis")]
    assert len(subd) == 2
    # variables store on Data Analysis node of debug tree
    if "Data" in subd[0]:
        subddata = subd[0]
        subdRTGPL = subd[1]
    else:
        subddata = subd[1]
        subdRTGPL = subd[0]
    assert len(subddata) == 5
    assert "DataAnalysis" in subddata
    assert "DataAnalysis.path" in subddata
    assert "Data.path" in subddata
    assert "RecordThatGetsParentsLater" in subddata

    prop = subddata["RecordThatGetsParentsLater"].get_property("someId")
    assert type(prop.value) == int
    assert prop.value == 23

    # record store on Data Analysis node of debug tree
    assert len(subdRTGPL) == 1
    prop2 = subdRTGPL["RecordThatGetsParentsLater"].get_property("someId")
    assert prop == prop2
