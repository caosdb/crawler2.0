Optional Features
=================

Email notifications
-------------------

The crawler can send email notifications if it made some changes or if
new data was inserted. This is (currently) only available if the crawler
runs as server side script of CaosDB. You need to add the following
section to your ``.pycaosdb.ini``

.. code:: ini

   [caoscrawler]
   send_crawler_notifications=True
   public_host_url=https://example..eu
   sendmail_to_address=someone@example.de
   sendmail_from_address=caosdb-no-reply@example.eu

This feature uses the ``sendmail`` functionality of
``caosadvancedtools``. Thus, it uses the setting

.. code:: ini

   [Misc]
   sendmail = /usr/sbin/sendmail
   #sendmail = /usr/local/bin/sendmail_to_file

to decide what tool is used for sending mails (use the upper one if you
want to actually send mails. See ``sendmail`` configuration in the
LinkAhead docs.

You can even supply the name of a custom CSS file that shall be used:

.. code:: ini

   [advancedtools]
   crawler.customcssfile = theme-research.css

Crawler Status Records
----------------------

The crawler can insert and update Records that contain essential
information about the data integration process. This is (currently) only
available if the crawler runs as server side script of CaosDB. To enable
this, add the following to your ``.pycaosdb.ini``

.. code:: ini

   [caoscrawler]
   create_crawler_status_records=True

You also need to add the data model needed for this as desribed by
``crawler_run_model.yml``.
