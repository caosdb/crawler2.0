Getting Started
+++++++++++++++

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   Installation<INSTALL>
   prerequisites
   helloworld
   optionalfeatures
   furtherreading

This section will help you get going! From the first installation steps to the first simple crawl.

Let's go!
