# Prerequisites

The CaosDB Crawler is a utility to create CaosDB Records from some data
structure, e.g. files, and synchronize these Records with a CaosDB server.

Thus two prerequisites to use the CaosDB Crawler are clear:
1. You need access to a running CaosDB instance. See the [documentation](https://docs.indiscale.com/caosdb-deploy/index.html).
2. You need access to the data that you want to insert, i.e. the files or
   the table from which you want to create Records.

Make sure that you configured your Python client to speak
to the correct CaosDB instance (see [configuration docs](https://docs.indiscale.com/caosdb-pylib/configuration.html)).

We would like to make another prerequisite explicit that is related to the first
point above: You need a data model. Typically, if you want to insert data into
an actively used CaosDB instance, there is a data model already. However, if
there is no data model yet, you can define one using the
[edit mode](https://docs.indiscale.com/caosdb-webui/tutorials/edit_mode.html)
or the [YAML format](https://docs.indiscale.com/caosdb-advanced-user-tools/yaml_interface.html).
We will provide small data models for the examples to come.


Also it is recommended, and necessary for the following chapters, that you have
some experience with the CaosDB Python client.
If you don't, you can start with
the [tutorials](https://docs.indiscale.com/caosdb-pylib/tutorials/index.html)

If you want to write CaosDB Crawler configuration files (so called CFoods), it helps if you know
regular expressions. If regular expressions are new to you, don't worry, we keep it simple in this
tutorial.
