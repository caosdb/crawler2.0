Further reading
===============

- A simple `documented example <https://gitlab.com/caosdb/documented-crawler-example>`_ which
  demonstrates the crawler usage.
- Some useful examples can be found in the `integration tests
  <https://gitlab.com/caosdb/caosdb-crawler/-/tree/main/integrationtests>`_ (and to a certain extent
  in the unit tests).
- TODO: Information on caching
