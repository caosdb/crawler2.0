CaosDB-Crawler Documentation
============================


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   Getting started<getting_started/index>
   Tutorials<tutorials/index>
   Concepts<concepts>
   Converters<converters/index>
   CFoods (Crawler Definitions)<cfood>
   Macros<macros>
   How to upgrade<how-to-upgrade>
   API documentation<_apidoc/modules>
   Related Projects<related_projects/index>

   Back to Overview <https://docs.indiscale.com/>

      

This is the documentation for CaosDB-Crawler (previously known as crawler 2.0) 
the main tool for automatic data insertion into CaosDB.

Its task is to automatically synchronize data found on file systems or in other
sources of data with the semantic data model of CaosDB.

More specifically, data that is contained in a hierarchical structure is converted to a data
structure that is consistent with a predefined semantic data model.

The hierarchical structure can be for example a file tree. However it can be
also something different like the contents of a JSON file or a file tree with
JSON files.

This documentation helps you to :doc:`get started<getting_started/index>`, explains the most important
:doc:`concepts<concepts>` and offers a range of :doc:`tutorials<tutorials/index>`.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
