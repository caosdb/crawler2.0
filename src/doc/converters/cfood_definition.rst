CFood definition
++++++++++++++++

Converter application to data is specified via a tree-like yml file (called ``cfood.yml``, by
convention).  The yml file specifies which Converters shall be used on which StructureElements, and
how to treat the generated *child* StructureElements.

The yaml definition may look like this:

.. todo::

  This is outdated, see ``cfood-schema.yml`` for the current specification of a ``cfood.yml``.

.. code-block:: yaml

    <NodeName>:
	type: <ConverterName>
	match: ".*"
	records:
	    Experiment1:
		parents:
		- Experiment
		- Blablabla
		date: $DATUM
		(...)
	    Experiment2:
		parents:
		- Experiment
	subtree:
	    (...)

The **<NodeName>** is a description of what the current block represents (e.g.
``experiment-folder``) and is used as an identifier.

**<type>** selects the converter that is going to be matched against
the current structure element. If the structure element matches (this
is a combination of a typecheck and a detailed match, see the
:py:class:`~caoscrawler.converters.converters.Converter` source
documentation for details), the converter will:

- generate records (with
  :py:meth:`~caoscrawler.converters.converters.Converter.create_records`)
- possibly process a subtree (with
  :py:meth:`~caoscrawler.converters.converters.Converter.create_children`)

**match** *TODO*

**records** is a dict of definitions that define the semantic structure
(see details below).

**subtree** makes the yaml recursive: It contains a list of new Converter
definitions, which work on the StructureElements that are returned by the
current Converter.
