Converters
))))))))))

Converters treat a StructureElement and during this process create a number of new
StructureElements: the children of the initially treated StructureElement.  Thus by treatment of
existing StructureElements, Converters create a tree of StructureElements.

.. image:: ../img/converter.png
  :height: 170
  :alt: Converters are Python classes that tell the crawler how to
        interprete StructureElements.

Each StructureElement in the tree has a set of properties, organized as
key-value pairs.
Some of those properties are specified by the type of StructureElement. For example,
a file could have the file name as property: ``'filename': myfile.dat``.
Converters may define additional functions that create further values. For
example, a regular expression could be used to get a date from a file name.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   CFood definition<cfood_definition>
   Standard converters<standard_converters>
   Further converters<further_converters>
   Custom converters<custom_converters>
   Transform functions<transform_functions>

