# Data Processing

The actual work is done in this `04_data_processing` folder. Depending on your
field and type and size of project, you can organize this folder in the way that
fits your process best. Here, a bit of chaos can happen ;) Keep in mind to
document your processing steps in the `02_materials_and_methods` folder and to
put in your final results into the `05_results` folder. In the end of your
project, it should be possible to delete everything in this folder and
reconstruct the working process using the documentation and raw data from
previous folders.
