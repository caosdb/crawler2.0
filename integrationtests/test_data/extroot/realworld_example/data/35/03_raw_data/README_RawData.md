# Raw Data

The `03_raw_data` folder is here to store all raw data of each dataset
associated with the project – the data that has not been edited by you yet but
which you plan to use in your research. It can be e.g. your unprocessed field
sampling records, or useful data from an online repository. Organize your data
in this folder in the following way:

- Each dataset should reside inside a subfolder. It is recommended to number and name these folders clearly, e.g. `03_raw_data/001_precipitationgermany2017`.

- **IMPORTANT**: provide the folder with information about your raw data by
  filling out a metadata form for each of your datasets! For this, 
  
  - either copy the `metadata-template.json` file and put it into your dataset
    folder. Open the copy with a text editor and fill out the fields. 
  - or use the metadata editor in the DataCoud web client (press the  "+" button
    and use "New matadata.json" file)

  If you can’t find information about your data to fill in here, you should
  reconsider using it - it is important to be able to trace your data sources to
  ensure a FAIR scientific process!

- For processing any of the data, make a copy of the dataset and paste it into
  the `04_data_processing` folder. This way, you make sure to keep your raw data
  in its original state.